<footer class="content-info" role="contentinfo">
  <div class="container pad-top">
  	<p>
		<span style="font-size:20px; color: #941646; font-weight: bold; text-align: left;">If you have questions, we're ready to help.  Call Bill Hand at <span style="color: #50576A">609-368-2135</span></span><br><br>Design © 2012 Referable.  Content © 2012 WJ Hand Builders, an Avalon NJ and PA area luxury home builder servicing <a href="http://wjhandbuilders.com/Avalon/" style="text-decoration:underline;">Avalon</a>, <a href="http://wjhandbuilders.com/stoneHarbor/" style="text-decoration:underline;">Stoneharbor</a>, <a href="http://wjhandbuilders.com/capemay/" style="text-decoration:underline;">Capemay</a>, <a href="http://wjhandbuilders.com/anglesea/" style="text-decoration:underline;">Anglesea</a>, <a href="http://wjhandbuilders.com/wildwood/" style="text-decoration:underline;">Wildwood</a>, <a href="http://wjhandbuilders.com/seaislecity/" style="text-decoration:underline;">Sea Isle City</a>, <a href="http://wjhandbuilders.com/oceanview/" style="text-decoration:underline;">Ocean View</a>, <a href="http://wjhandbuilders.com/oceancity/" style="text-decoration:underline;">Ocean City</a>, <a href="http://wjhandbuilders.com/Wayne/" style="text-decoration:underline;">Wayne</a>, <a href="http://wjhandbuilders.com/brynmawr/" style="text-decoration:underline;">Bryn Mawr</a> and neighboring areas.  WJ Hand Builders is located at 5 Hallman Road, Stone Harbor NJ 08247.  <a href="http://wjhandbuilders.com/contact-us/" style="text-decoration:underline;">Contact us</a> to talk with our custom home builders about your project.
		<p>&nbsp;</p>
		Powered by <a href="http://competeleap.com" style="color:#157BA5;">CompleteLeap</a>
	</p>
    <?php dynamic_sidebar('sidebar-footer'); ?>

  </div>
</footer>
<p>&nbsp;</p>
