<div id="carousel-id" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel-id" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
        <li data-target="#carousel-id" data-slide-to="2" class=""></li>
        <li data-target="#carousel-id" data-slide-to="3" class=""></li>
        <li data-target="#carousel-id" data-slide-to="4" class=""></li>
        <li data-target="#carousel-id" data-slide-to="5" class=""></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_one_sm.jpg">
            <div class="container">
                <div class="carousel-caption carousel-caption-option">
                    
                    <!-- <h1>Call us at 215-557-1555 or <a href="/contact-us">Schedule Online</a> today.</h1> -->
                    <!-- <p class="large">We are committed to providing a relaxing visit that will leave you smiling.  Call us at <a href="tel:2155571555" onclick="_gaq.push(['_trackEvent','Phone Call Tracking','Click/Touch','Mobile']);">(215) 557-1555</a> </p>
                	<a class="btn btn-info btn-lg" role="button" href="http://drstevenrice.com/contact-us">Schedule an Appointment</a> -->
                </div>
            </div>
        </div>
        <div class="item">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_two_sm.jpg">
            <div class="container">
                <div class="carousel-caption">
                    <!-- <h1>Call us at 215-557-1555 or <a href="/contact-us">Schedule Online</a> today.</h1> -->
                    <!-- <p class="large">Dr. Rice loves to spend time with each individual patient to make sure that he and his team have not only met, but exceeded every expectation of each patient's visit.  Call us at <a href="tel:2155571555" onclick="_gaq.push(['_trackEvent','Phone Call Tracking','Click/Touch','Mobile']);">(215) 557-1555</a> </p>
                	<a class="btn btn-info btn-lg" role="button" href="http://drstevenrice.com/contact-us">Schedule an Appointment</a> -->
                </div>
            </div>
        </div>

        <div class="item">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_three_sm.jpg">
            
        </div>

        <div class="item">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_four_sm.jpg">
            
        </div>

        <div class="item">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_five_sm.jpg">
            
        </div>

        <div class="item">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel_six_sm.jpg">
            
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>