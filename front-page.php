<?php
/*
Template Name: Home Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
  <?php //get_template_part('templates/page', 'header'); ?>
  <?php //get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php get_template_part('templates/carousel'); ?>

<p>&nbsp;</p>

<div class="row">
	<div class="col-lg-6 text-center">
		<img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/remodeling1.gif">
		<p></p>
		<h3><a href="/remodeling">Remodeling</a></h3>
		<p></p>
		<p class="text-justify">We have the team and expertise to tackle design services for any project, from bathroom or kitchen remodeling all the way through full home updates and additions.</p>
	</div>

	<div class="col-lg-6 text-center">
		<img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/newhomes2.gif">
		<p></p>
		<h3><a href="/new-construction">New Homes</a></h3>
		<p></p>
		<p class="text-justify">From first groundbreaking until the last trim installation, our custom built homes are designed with your vision in mind, so you can live in a home that is truly your own.</p>
	</div>

	<div class="col-lg-6 text-center">
		<img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/contracting3.gif">
		<p></p>
		<h3><a href="/general-contracting">Contracting</a></h3>
		<p></p>
		<p class="text-justify">Bill Hand knows what it takes to make a project successful every step of the way. All the way through from planning to completion, you will be in the hands of a professional.</p>
	</div>

	<div class="col-lg-6 text-center">
		<img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery4.gif">
		<p></p>
		<h3><a href="/home-construction-gallery">Gallery</a></h3>
		<p></p>
		<p class="text-justify">After 40 years of luxury home craftsmanship, we have a large portfolio of client project photos...and these are just a sampling. The possibilities for your home are limitless!</p>
	</div>

</div>

<div class="row">
	<div class="col-lg-12">
		<hr>
		<h3>Let’s Connect</h3>
		<p class="text-justify">WJ Hand Builders offers storm proofing for New Jersey shore residents and home owners. We have experienced firsthand the devastation that Hurricane Sandy has brought on the East Coast of the United States. Our hearts and minds go out to our friends, family and neighbors up and down our beautiful coastline.</p>
		<p class="text-justify">Turn A House Into A Home We at WJ Hand realize that there is a difference between a house and a home. We strongly believe that a hands on approach is the best way to make sure that we create the home of your dreams. Whether you need a kitchen remodel, your basement finished, a new bedroom, a sunroom constructed or a complete home renovation, you can count on WJ Hand to bring your vision to life.</p>
	</div>
	
</div>

